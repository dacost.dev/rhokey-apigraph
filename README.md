# Rhokey Graphql API

_Open source graphql API_

---

#### Prerequisites 📋

_Node_

---

## Source Code 🛠️

#### Clone the project 🔧

```
git clone git@gitlab.com:dacost.dev/rhokey-apigraph.git
```

#### Install dependecies 📖

_Yarn_

```
yarn
```

_Npm_

```
npm i
```

#### Create .env file

_Change the values to your auth0 & mongodb account service_

```
CLIENT_ID='CLIENT ID AUTH0'
ISSUER_BASE_URL='ISSUER URL AUTH0'
SECRET='SECRET KEY AUTH0'
PORT=4000
MONGODB_URI='URL CONNECTION MONGODB'
AUTH0_URI='URL AUTH0 TOKEN'
AUDIENCE_URI='AUDIENCE URL AUTH0'
```

### Run project ⚙️

_Yarn_

```
yarn start
```

_Npm_

```
npm start
```

---

### Playground

_In the browser_

```
http://localhost:4000/playground
```

## Authors ✒️

- **Diego Acosta** - _Mantainer & owner_ - <dacost.dev@gmail.com> - [dacost](https://gitlab.com/dacost.dev)
- **Brandon Jimenez** - _Mantainer & owner_ - <brandon.jimenezdbs@gmail.com> - [brandhi](https://gitlab.com/Brandhi)

---

## License 📄

Rhokey Graphql API is open source software licensed as MIT. - [LICENSE.md](LICENSE.md)

---

## Extra 🎁

- Let's not drop the support between human beens 📢
- Let's make education free ☕
- Smoke & Code 🍁🤓⌚ 4:20.

---
