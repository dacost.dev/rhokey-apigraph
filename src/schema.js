const { buildSchema } = require("graphql");

const schema = buildSchema(`

  type User {
    id: ID!
    email: String!
    password: String!
    name: String
    userName: String
    firstName: String
    lastName: String
    birthday: String
    phone: String
  }

  type Login {
    email: String
    password: String
  }

  type Query {
    users: [User!]!
    user(id: String!): User!
    login: Login!
  }

  type Mutation {
    User(
    name: String!,
    userName: String!,
    firstName: String!,
    lastName: String!,
    birthday: Int!,
    phone: Int!,
    email: String!): User!
  }
`);

module.exports = schema;
