// src/database.js

require("dotenv").config();
const bcrypt = require("bcrypt");
const { MongoClient, ServerApiVersion } = require("mongodb");
const { MongoMemoryServer } = require("mongodb-memory-server");

let database = null;

async function startDatabase() {
  const mongo = new MongoMemoryServer();
  const uri = process.env.MONGODB_URI;
  const connection = await MongoClient.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverApi: ServerApiVersion.v1,
  });

  database = connection.db();

  // comment line: database = connection.db() & discomment the next lines to create new forced user
  // need to comment the if error scope of users resolver in index.js

  // const pass = "access123";
  // let hashedPassword = null;
  // const saltRounds = 10;

  // if (!database) {
  //   database = connection.db();

  //   bcrypt.hash(pass, saltRounds, function (err, hash) {
  //     database.collection("users").insertMany([
  //       {
  //         email: "admin@gmail.com",
  //         password: hash,
  //         name: "admin",
  //         userName: "admin",
  //         firstName: "admin",
  //         lastName: "admin",
  //         birthday: "10/10/2010",
  //         phone: "73847382",
  //       },
  //     ]);
  //   });
  // }

  return database;
}

module.exports = startDatabase;
