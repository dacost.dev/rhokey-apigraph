// src/index.js

require("dotenv").config();
const bcrypt = require("bcrypt");
const express = require("express");
const bodyParser = require("body-parser");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema");
const startDatabase = require("./database");
const expressPlayground =
  require("graphql-playground-middleware-express").default;
const isTokenValid = require("./validate");
var request = require("request");

const contextPublic = async (req) => {
  const db = await startDatabase();
  return { db };
};

const context = async (req) => {
  const db = await startDatabase();
  const { authorization: token } = req.headers;

  return { db, token };
};

const validateUser = async (email) => {
  const { db } = await contextPublic();

  return db.collection("users").find({ email: email });
};

const resolvers = {
  users: async (_, context) => {
    const { db, token } = await context();

    const { error } = await isTokenValid(token);

    if (error) {
      throw new Error(error);
    }
    return db.collection("users").find().toArray();
  },
  user: async ({ id }, context) => {
    const { db, token } = await context();

    const { error } = await isTokenValid(token);

    if (error) {
      throw new Error(error);
    }
    return db.collection("users").findOne({ id });
  },
  editUser: async ({ id, title, description }, context) => {
    const { db, token } = await context();

    const { error } = await isTokenValid(token);

    if (error) {
      throw new Error(error);
    }

    return db
      .collection("users")
      .findOneAndUpdate(
        { id },
        { $set: { password } },
        { returnOriginal: false }
      )
      .then((resp) => resp.value);
  },
};

const app = express();

app.use(
  "/graphql",
  graphqlHTTP(async (req) => ({
    schema,
    rootValue: resolvers,
    context: () => context(req),
  }))
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/playground", expressPlayground({ endpoint: "/graphql" }));

app.post("/login", (req, res) => {
  validateUser(req.body.email).then((x) => {
    x.toArray().then((y) => {
      if (y.length > 0) {
        var options = {
          method: "POST",
          url: process.env.AUTH0_URI,
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.SECRET,
            audience: process.env.AUDIENCE_URI,
            grant_type: "client_credentials",
          }),
        };

        request(options, function (error, response, body) {
          if (error) throw new Error(error);

          bcrypt.compare(
            req.body.password,
            y[0].password,
            function (err, result) {
              if (result) {
                res.send(body);
              } else {
                res.send({
                  error: "password invalido",
                });
              }
            }
          );
        });
      } else {
        res.send({ error: "No existe usuario" });
      }
    });
  });
});

app.listen(process.env.PORT);

console.log(`🚀 Server ready at http://localhost:${process.env.PORT}/graphql`);
